import {
  FETCH_POSTS,
  ADD_POST,
  DELETE_POST,
  EDIT_POST,
  UPDATE_POST,
  TOGGLE_LIKE,
} from "../actions/actionTypes";

const postReducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_POSTS:
      action.posts.forEach((post) => (post.isLiked = true));
      return {
        ...state,
        posts: action.posts,
      };
    case ADD_POST:
    action.post.id = state.posts.length + 1;
      action.post.isCanDeleted = true;
      state.posts.forEach((post) => (post.isCanDeleted = false));
      return {
        ...state,
        posts: [action.post, ...state.posts],
      };
    case EDIT_POST:
      return {
        ...state,
        posts: state.posts.map((post) =>
          post.id === action.postId ? { ...post, editing: true } : post
        ),
      };
    case TOGGLE_LIKE:
    return {
        ...state,
        posts: state.posts.map((post) =>
          post.id === action.postId ? { ...post, isLiked: !post.isLiked } : post
        ),
      };
    case UPDATE_POST:
      return {
        ...state,
        posts: state.posts.map((post) =>
          post.id === action.post.postId
            ? {
                ...post,
                body: action.post.body,
                imageId: action.post.imageId,
                editing: !post.editing,
              }
            : post
        ),
      };
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter((post) => post.id !== action.postId),
      };
    default:
      return state;
  }
};

export default postReducer;
