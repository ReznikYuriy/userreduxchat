import React from "react";
import "./message-list.css";
import Post from "../Post";

const MessageList = ({ data, currentUserId, deletePost, toggleLike }) => {
  return (
    <div className="message-list">
      {data.map((post) => (
        <Post
          key={post.id}
          post={post}
          currentUserId={currentUserId}
          deletePost={deletePost}
          toggleLike={toggleLike}
        />
      ))}
    </div>
  );
};

export default MessageList;
