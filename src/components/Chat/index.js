import React, { useState, useEffect } from "react";
import "./chat.css";
import Spinner from "../spinner/";
import Header from "../header";
import MessageList from "../message-list";
import MessageInput from "../message-input";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchPosts, deletePost, addPost, toggleLike } from "../../actions/actions";
import {CHAT_NAME, CURRENT_USER_ID} from '../../actions/config';
import {getParticipantsCount, getLastMessageTime, getMessagesCount} from '../../actions/selectors';


function Chat({
  fetchPosts: loadAllPost,
  addPost: addpost,
  deletePost: delPost,
  toggleLike:like,
  data=[],
}) {
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    loadAllPost();
    setLoaded(true);
  }, []);

  return (
    <div className="chat">
      {!isLoaded && <Spinner />}
      {isLoaded && (
        <>
         { <Header
            name={CHAT_NAME}
            nParticipants={getParticipantsCount(data)}
            nMessages={getMessagesCount(data)}
            lastMessage={getLastMessageTime(data)}
          />}
          <MessageList
            data={data}
            currentUserId={CURRENT_USER_ID}
            deletePost={delPost}
            toggleLike={like}
          />
          <MessageInput addPost={addpost} />
        </>
      )}
    </div>
  );
}
const mapStateToProps = state => ({
  data: state.posts
});
const actions = {
  fetchPosts,
  deletePost,
  addPost,
  toggleLike
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
