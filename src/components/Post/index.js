import React from "react";
import moment from "moment";

import "./post.css";

const Post = ({ post, currentUserId, deletePost, toggleLike }) => {
  const { avatar, user, text, createdAt, id, userId } = post;
  const date = moment(createdAt).fromNow();

  const isMyOwnPost = currentUserId === userId ? true : false;
  let postLikeStyle = post.isLiked ? "up" : "down";
  let postPositionStyle = isMyOwnPost ? "myown-card" : "default-card";
  return (
    <div className={`${postPositionStyle} card-container`}>
      <div className="card-content">
        {!isMyOwnPost && (
          <img className="card-img" size="mini" src={avatar} alt="avatar" />
        )}
        <div className="card-content-header">{user}</div>
        <div className="card-content-meta">posted {date}</div>
        <div className="card-description">
          <strong>{text}</strong>
        </div>
      </div>
      <div className="card-extra-content">
        <span>
          {isMyOwnPost ? (
            <i
              className={`fa fa-thumbs-${postLikeStyle}`}
              onClick={() => {}}
            ></i>
          ) : (
            <i
              className={`fa fa-thumbs-${postLikeStyle}`}
              onClick={() => toggleLike(id)}
            ></i>
          )}
        </span>
        {isMyOwnPost && (
          <span className='service-icon'>
            <i className="fa fa-pencil"></i>
          </span>
        )}
        {post.isCanDeleted && isMyOwnPost && (
          <span className='service-icon'>
            <i className="fas fa-trash-alt" onClick={() => deletePost(id)}></i>
          </span>
        )}
      </div>
    </div>
  );
};

export default Post;
