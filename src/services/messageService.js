const _apiBase = "https://edikdolynskyi.github.io/react_sources/";

const getResource = async (url) => {
  const res = await fetch(`${_apiBase}${url}`);

  if (!res.ok) {
    throw new Error(`Could not fetch ${url}, received ${res.status}`);
  }
  return await res.json();
};

export const getAllMessages = async () => {
  const res = await getResource('messages.json');
  return res;
};
