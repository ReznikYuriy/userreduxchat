export const ADD_POST = "ADD_POST";
export const DELETE_POST = "DELETE_POST";
export const EDIT_POST = "EDIT_POST";
export const UPDATE_POST = "UPDATE_POST";
export const GET_ALL_POSTS = "GET_ALL_POSTS";
export const TOGGLE_LIKE = "TOGGLE_LIKE";
export const FETCH_POSTS = "FETCH_POSTS";
