import { getAllMessages } from "../services/messageService";
import * as conf from "./config";
import {
  ADD_POST,
  DELETE_POST,
  EDIT_POST,
  UPDATE_POST,
  TOGGLE_LIKE,
  FETCH_POSTS,
} from "./actionTypes";

const fetchPostsAction = (posts) => ({
  type: FETCH_POSTS,
  posts,
});

const addPostAction = (post) => ({
  type: ADD_POST,
  post,
});

const deletePostAction = (postId) => ({
  type: DELETE_POST,
  postId,
});

const toggleLikeAction = (postId) => ({
  type: TOGGLE_LIKE,
  postId,
});
/*******************************************************/
export const fetchPosts = () => async (dispatch) => {
  const posts = await getAllMessages();
  dispatch(fetchPostsAction(posts));
};
export const addPost = (body) => (dispatch) => {
  const newPost = {
    userId: conf.CURRENT_USER_ID,
    avatar: conf.CURRENT_USER_AVATAR,
    user: conf.CURRENT_USER_NAME,
    text: body,
    createdAt: new Date(Date.now()).toISOString(),
    editedAt: "",
    isLiked: true,
  };
  dispatch(addPostAction(newPost));
};
export const deletePost = (postId) => (dispatch) => {
  dispatch(deletePostAction(postId));
};

export const toggleLike = (postId) => (dispatch) => {
  dispatch(toggleLikeAction(postId));
};
