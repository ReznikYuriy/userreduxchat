export const getParticipantsCount = (state) => {
    const set = new Set();
    state.forEach((element) => {
      set.add(element.userId);
    });
    return set.size;
  };
export const getLastMessageTime = (state) => {
    const createdAts = state.map((elem) => Date.parse(elem.createdAt));
    const max = Math.max(...createdAts);
    return new Date(max).toLocaleTimeString().slice(0, -3);
  };

  export const getMessagesCount = (state) => {
    return state.length;
  };