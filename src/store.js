import { createStore, applyMiddleware } from 'redux';
import postReducer from './reducers/postReducer';
import thunk from 'redux-thunk';

const initialState = [];

const store = createStore(
  postReducer,
  initialState,
  applyMiddleware(thunk)
);

export default store;