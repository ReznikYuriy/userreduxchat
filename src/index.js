import React from "react";
import { render } from "react-dom";
import "./index.css";
import Chat from "./components/Chat";
import { Provider } from "react-redux";
import store from "./store";

render(
  <Provider store={store}>
    <Chat />
  </Provider>,
  document.getElementById("root")
);
